package com.epamlab.utils;

import java.lang.annotation.Annotation;
import java.lang.reflect.Proxy;

/**
 * Created by Admin on 14.07.2017.
 */
public final class InstanceOfClassFactory {
    private InstanceOfClassFactory() {
    }

    /**
     * @param <T> sf
     * @param aClass Class whose object we want to receive
     * @return Object of aClass
     */
    public static <T> T getInstanceOf(final Class<T> aClass) {
        try {
            Annotation proxy = aClass.getAnnotation(com.epamlab.annotation.Proxy.class);
            if (proxy == null) {
                return aClass.newInstance();
            }
            return (T) Proxy.newProxyInstance(aClass.getClassLoader(),
                    aClass.getInterfaces(), new BeforeAfterHandler(aClass.newInstance()));
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
            throw  new RuntimeException(e);
        }
    }
}

