package com.epamlab.utils;

import com.epamlab.annotation.Equal;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;


/**
 * Created by Admin on 16.07.2017.
 */
public final class EqualAnalyzer {

    private EqualAnalyzer() {
    }

    /**
     * unified objects equality analyzer
     * @param o1 First object for equals
     * @param o2 Second object for equals
     * @return true if objects are equals, else false
     */
    public static boolean equalObjects(final Object o1, final Object o2) {
        if (o1 == null || o2 == null) {
            return false;
        }
        Class<?> c1 = o1.getClass();
        Class<?> c2 = o2.getClass();
         if (o2.getClass() != null && o1.getClass() != null
                && !(c1.isInstance(o2) || c2.isInstance(o1))) {
            return false;
        }

        Set<Field> annotatedFields1 = findFields(c1);
        Set<Field> annotatedFields2 = findFields(c2);
        if (annotatedFields1.size() != annotatedFields2.size()) {
            return false;
        }
        return equalFields(annotatedFields1, o1, o2);
    }

    private static Set<Field> findFields(final Class<?> classs) {
        Set<Field> set = new HashSet<>();
        Class<?> c = classs;
        while (c != null) {
            for (Field field : c.getDeclaredFields()) {
                if (field.isAnnotationPresent(Equal.class)) {
                    set.add(field);
                }
            }
            c = c.getSuperclass();
        }
        return set;
    }

    private static boolean equalFields(final Set<Field> fields1, final Object o1, final Object o2) {

        Iterator<Field> it1 = fields1.iterator();
        while (it1.hasNext()) {
            Field field = it1.next();
            int compareby = field.getAnnotation(Equal.class).compareby();
            if (compareby == Equal.REFERENCE) {
                if (runGetter(field, o1) != runGetter(field, o2)) {
                    return false;
                }
            } else {
                if (!runGetter(field, o1).equals(runGetter(field, o2))) {
                    return false;
                }
            }
        }
        return true;

    }

    /**
     *
     * @param field asd
     * @param o asd
     * @return asd
     */
    public static Object runGetter(final Field field, final Object o) {
        // MZ: Find the correct method
        for (Method method : o.getClass().getMethods()) {
            if ((method.getName().startsWith("get")) && (method.getName().length() == (field.getName().length() + 3))) {
                if (method.getName().toLowerCase().endsWith(field.getName().toLowerCase())) {
                    // MZ: Method found, run it
                    try {
                        return method.invoke(o);
                    } catch (IllegalAccessException e) {
                    } catch (InvocationTargetException e) {
                    }

                }
            }
        }
        return null;
    }


}
