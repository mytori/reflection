package com.epamlab.utils;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Created by Admin on 15.07.2017.
 */
public class BeforeAfterHandler implements InvocationHandler {
    private Object obj;

    /**
     *
     * @param obj Wrapped object
     */
    public BeforeAfterHandler(final Object obj) {
        this.obj = obj;
    }

    @Override
    public Object invoke(final Object proxy, final Method method, final Object[] args) throws Throwable {
        Object result;
        try {
            System.out.println("before method " + method.getName());
            result = method.invoke(obj, args);
        } catch (InvocationTargetException e) {
            throw  e.getTargetException();
        } catch (Exception e) {
            throw new RuntimeException("unexpected invocation exception: " + e.getMessage());
        } finally {
            System.out.println("After method " + method.getName());
        }
        return result;
    }
}
