package com.epamlab.beans;

import com.epamlab.annotation.Proxy;
import com.epamlab.utils.BeforeAfterHandler;

/**
 * Created by Admin on 18.07.2017.
 */
@Proxy(invocationHandler = BeforeAfterHandler.class)
public class AdvancedUser implements IUser {

   @Override
   public void someMethod() {
      System.out.println("AdvancedUser someMethod");
   }
}
