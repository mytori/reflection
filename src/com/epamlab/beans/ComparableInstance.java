package com.epamlab.beans;

import com.epamlab.annotation.Equal;

/**
 * Created by Admin on 15.07.2017.
 */

public class ComparableInstance {
    @Equal(compareby = Equal.VALUE)
    private int value;
    @Equal(compareby = Equal.REFERENCE)
    private IUser user;

    /**
     *
     * @param value Some int value
     * @param user IUser
     */
    public  ComparableInstance(final int value, final IUser user) {
        this.value = value;
        this.user = user;
    }

    /**
     * Default constructor
     */
    public ComparableInstance() {
    }

    /**
     *
     * @return Some int value
     */
    public int getValue() {
        return value;
    }

    /**
     * Value setter
     * @param value some int value
     */
    public void setValue(final int value) {
        this.value = value;
    }

    /**
     * User setter
     * @param user IUser
     */
    public void setUser(final IUser user) {
        this.user = user;
    }

    /**
     * User getter
     * @return IUser
     */
    public IUser getUser() {
        return user;
    }

}
