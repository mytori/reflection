package com.epamlab;

import com.epamlab.beans.AdvancedUser;
import com.epamlab.beans.ComparableInstance;
import com.epamlab.beans.IUser;
import com.epamlab.beans.User;
import com.epamlab.utils.EqualAnalyzer;
import com.epamlab.utils.InstanceOfClassFactory;

/**
 * Created by Admin on 15.07.2017.
 */
public final class Runner {
    private Runner() {
    }

    /**
     * @param args Command line args
     */
    public static void main(final String[] args) {
        IUser user = InstanceOfClassFactory.getInstanceOf(User.class);
        IUser advancedUser = InstanceOfClassFactory.getInstanceOf(AdvancedUser.class);

        user.someMethod();
        advancedUser.someMethod();

        ComparableInstance i1 = new ComparableInstance(1, user);
        ComparableInstance i2 = new ComparableInstance(1, advancedUser);
        ComparableInstance i3 = new ComparableInstance(1, advancedUser);
        System.out.println(EqualAnalyzer.equalObjects(i1, i2));
        System.out.println(EqualAnalyzer.equalObjects(i3, i2));
    }
}
