package com.epamlab.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by Admin on 13.07.2017.
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface Proxy {
    /**
     *
     * @return  Class for using as handler
     */
    Class invocationHandler();
}
