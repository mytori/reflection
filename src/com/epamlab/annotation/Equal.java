package com.epamlab.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by Admin on 16.07.2017.
 */

@Retention(RetentionPolicy.RUNTIME)
public @interface Equal {
    /**
     * reference by reference
     */
    int REFERENCE = 0;
    /**
     * equal by value
     */
    int VALUE = 1;
    /**
     *
     * @return compareby REFERENCE or VALUE
     */
    int compareby();
}
